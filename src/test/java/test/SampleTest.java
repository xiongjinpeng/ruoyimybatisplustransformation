package test;


import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.ruoyi.RuoYiApplication;
import com.ruoyi.project.system.domain.RoleMenu;
import com.ruoyi.project.system.domain.User;
import com.ruoyi.project.system.mapper.MenuMapper;
import com.ruoyi.project.system.mapper.RoleDeptMapper;
import com.ruoyi.project.system.mapper.RoleMapper;
import com.ruoyi.project.system.mapper.RoleMenuMapper;
import com.ruoyi.project.system.mapper.UserMapper;
import com.ruoyi.project.system.mapper.UserPostMapper;
import com.ruoyi.project.system.mapper.UserRoleMapper;
import com.ruoyi.project.system.service.IUserService;

/**
 * 启动程序
 * 
 * @author ruoyi
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=RuoYiApplication.class)
public class SampleTest {

    @Resource private MenuMapper menuMapper;
    @Autowired private UserMapper userMapper;
    @Autowired private IUserService userService;
    @Autowired private UserPostMapper userPostMapper;
    @Autowired private UserRoleMapper userRoleMapper;
    
    @Autowired private RoleDeptMapper roleDeptMapper;
    @Autowired private RoleMapper roleMapper;
    @Autowired private RoleMenuMapper roleMenuMapper;

    @Test
    public void testSelect() {
    	
        System.out.println(("----- selectAll method test ------"));
//        List<Menu> userList = menuMapper.selectList(null);
//        userList.forEach(System.out::println);
//        List<User> userList = userService.list();
//        userList.forEach(System.out::println);
        User u = new User();
        u.setDeptId(2L);
        u.setLoginName("aaaaaaaaaaaaaaa2");
        u.setUserName("gggggggggggggggggggg2");
        userService.save(u);
        System.out.println(u.getUserId());
    }
}